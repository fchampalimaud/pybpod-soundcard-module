.. SoundCardModule documentation master file, created by
   sphinx-quickstart on Thu Nov 29 09:23:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

What is the PyBpod's Sound Card Module?
=======================================
The Harp Sound Card is a board developed by the Scientific Hardware Platform
at the Champalimaud Foundation.

This documentation is for the Python 3 library to control the
**pybpod_soundcard_module** module.


Table of contents
-----------------

.. toctree::
    :maxdepth: 2
    :includehidden:
    :caption: First steps

    getting-started
    user-interface

.. toctree::
    :maxdepth: 2
    :includehidden:
    :caption: API reference

    SoundCard USB connection <usb-api>
    PyBpod API <pybpod-api>
